from django.contrib import admin
from coredrca.models import *

# Register your models here.
admin.site.register(Aluno)
admin.site.register(Departamento)
admin.site.register(Professor)
admin.site.register(Curso)
admin.site.register(Disciplina)
admin.site.register(Credito)
admin.site.register(Secretaria)