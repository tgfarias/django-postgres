# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.http.response import HttpResponse
from django.template import RequestContext, loader
from coredrca.models import Aluno

# Create your views here.

def home(request):
    alunos = Aluno.objects.all().order_by('nome')
    template = loader.get_template('index.html')
    context = RequestContext(request,{'alunos':alunos})
    return HttpResponse(template.render(context))

